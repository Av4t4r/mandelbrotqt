#-------------------------------------------------
#
# Project created by QtCreator 2015-12-19T18:12:15
#
#-------------------------------------------------

QT += widgets

HEADERS       = mandelbrotwidget.h \
                renderthread.h \
    mandelbrotwidgetopengl.h
SOURCES       = main.cpp \
                mandelbrotwidget.cpp \
                renderthread.cpp \
    mandelbrotwidgetopengl.cpp

unix:!mac:!vxworks:!integrity:!haiku:LIBS += -lm

TRANSLATIONS = es.ts\
               en.ts

QMAKE_EXTRA_COMPILERS += lrelease
lrelease.input         = TRANSLATIONS
lrelease.output        = ${QMAKE_FILE_BASE}.qm
lrelease.commands      = $$[QT_INSTALL_BINS]/lrelease ${QMAKE_FILE_IN} -qm ${QMAKE_FILE_BASE}.qm
lrelease.CONFIG       += no_link target_predeps
