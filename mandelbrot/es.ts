<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es" sourcelanguage="en">
<context>
    <name>MandelbrotWidget</name>
    <message>
        <location filename="mandelbrotwidget.cpp" line="66"/>
        <source>Mandelbrot</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mandelbrotwidget.cpp" line="81"/>
        <source>Rendering initial image, please wait...</source>
        <translation>Preparando la imágen inicial, por favor aguarde...</translation>
    </message>
    <message>
        <location filename="mandelbrotwidget.cpp" line="102"/>
        <source>Use mouse wheel or the &apos;+&apos; and &apos;-&apos; keys to zoom. Press and hold left mouse button to scroll.</source>
        <translation>Utilize la rueda del mouse, o las teclas &apos;+&apos; y &apos;-&apos; para hacer zoom. Presione y arrastre para mover la imágen.</translation>
    </message>
</context>
</TS>
